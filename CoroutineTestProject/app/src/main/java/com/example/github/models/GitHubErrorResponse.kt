package com.example.github.models

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class GitHubErrorResponse(
    val message: String,
    val errors: List<GitHubError>?
)