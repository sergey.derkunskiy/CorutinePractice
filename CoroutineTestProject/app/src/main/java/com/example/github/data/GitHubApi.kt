package com.example.github.data


import com.example.github.models.GitHubRepo
import com.example.github.models.GitHubUser
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.*

interface GitHubApi {
    @Headers("Accept: application/vnd.github.v3+json")
    @GET("user")
    suspend fun getCurrentUser(): GitHubUser

    @Headers("Accept: application/vnd.github.v3+json")
    @GET("/user/following")
    suspend fun getCurrentUserFollows(): List<GitHubUser>

    @Headers("Accept: application/vnd.github.v3+json")
    @GET("repositories")
    suspend fun getReposList(): List<GitHubRepo>

    @Headers("Accept: application/vnd.github.v3+json")
    @GET("/user/starred/{owner}/{repo}")
    fun checkIfRepoStarred(
            @Path("owner") owner: String,
            @Path("repo") repo: String
    ): Call<ResponseBody>

    @Headers("Accept: application/vnd.github.v3+json")
    @PUT("/user/starred/{owner}/{repo}")
    suspend fun starRepo(
            @Path("owner") owner: String,
            @Path("repo") repo: String
    ): Response<Unit>

    @Headers("Accept: application/vnd.github.v3+json")
    @DELETE("/user/starred/{owner}/{repo}")
    suspend fun unStarRepo(
            @Path("owner") owner: String,
            @Path("repo") repo: String
    ): Response<Unit>


}