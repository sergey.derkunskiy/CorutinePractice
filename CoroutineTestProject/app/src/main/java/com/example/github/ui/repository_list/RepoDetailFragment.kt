package com.example.github.ui.repository_list

import android.app.AlertDialog
import android.app.Dialog
import android.os.Bundle
import android.util.Log
import android.widget.ImageView
import android.widget.TextView
import androidx.core.view.isVisible
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.observe
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.github.R
import com.example.github.models.GithubViewModelMain
import com.example.github.models.GitHubRepo


class RepoDetailFragment : DialogFragment(

) {
    private lateinit var avatar: ImageView
    private lateinit var star: ImageView
    private lateinit var id: TextView
    private lateinit var repoName: TextView
    private lateinit var repoFullNameET: TextView
    private lateinit var repoDescET: TextView
    private lateinit var repoHtmlUrlET: TextView
    private lateinit var repoSshUrlET: TextView

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return activity?.let {
            val builder = AlertDialog.Builder(it)
            // Get the layout inflater
            val inflater = requireActivity().layoutInflater
            val view = inflater.inflate(R.layout.fragment_repo_detail, null)
            avatar = view.findViewById(R.id.avatar_imageView)
            star = view.findViewById(R.id.star)
            id = view.findViewById(R.id.id_text)
            repoName = view.findViewById(R.id.name_rep)
            repoFullNameET = view.findViewById(R.id.full_name)
            repoDescET = view.findViewById(R.id.description)
            repoHtmlUrlET = view.findViewById(R.id.url_text)
            repoSshUrlET = view.findViewById(R.id.ssh_url_text)



            builder.setView(view)
                    // Add action buttons
                    .setPositiveButton("OK") { dialog, _ ->
                        dialog.cancel()
                    }

            GithubViewModelMain.currentRepo.observe(this) { currentRepo ->
                setRepoToUI(currentRepo)
            }

            star.setOnClickListener {
                GithubViewModelMain.reverseStar()
                Log.e("AAAAAAAAAa","Start is push!")
            }
            builder.create()


        } ?: throw IllegalStateException("Activity cannot be null")
    }

    private fun setRepoToUI(currentRepo: GitHubRepo) {
        Glide.with(this)
                .load(currentRepo.owner.avatar_url)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .placeholder(R.drawable.ic_avatar)
                .error(R.drawable.ic_avatar)
                .into(avatar)
        repoName.text = currentRepo.name
        repoFullNameET.text = currentRepo.full_name
        id.text =currentRepo.id.toString()
        repoHtmlUrlET.text =currentRepo.html_url
        currentRepo.description.orEmpty().let {
            if (it.isNotEmpty()) {
                repoDescET.text =it
                repoDescET.isVisible = true
            } else repoDescET.isVisible = false
        }
        currentRepo.ssh_url.orEmpty().let {
            if (it.isNotEmpty()) {
                repoSshUrlET.text =it
                repoSshUrlET.isVisible = true
            } else repoSshUrlET.isVisible = false
        }
        currentRepo.starred.let {
            when (it) {
                true -> star.setImageResource(R.drawable.ic_star_full)
                else -> star.setImageResource(R.drawable.ic_star)
            }
        }
    }


}
