package com.example.github.models

import android.util.Log
import androidx.annotation.StringRes
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.github.utils.SingleLiveEvent
import kotlinx.coroutines.*

object GithubViewModelMain : ViewModel() {

    private val repository = GitHubRepository()
    private val fragmentScope = CoroutineScope(SupervisorJob() + Dispatchers.Main)
    private val fragmentScope2 = CoroutineScope(SupervisorJob() + Dispatchers.Unconfined)

    private val currentUserLiveData = MutableLiveData<GitHubUser>()
    val currentUser: LiveData<GitHubUser>
        get() = currentUserLiveData

    private val currentUserFollowsLiveData = MutableLiveData<List<GitHubUser>>()
    val currentUserFollows: LiveData<List<GitHubUser>>
        get() = currentUserFollowsLiveData

    private val currentRepoLiveData = MutableLiveData<GitHubRepo>()
    val currentRepo: LiveData<GitHubRepo>
        get() = currentRepoLiveData

    private val errorLiveData = SingleLiveEvent<String>()
    val error: LiveData<String>
        get() = errorLiveData

    private val publicReposLiveData = MutableLiveData<List<GitHubRepo>>()
    val publicRepos: LiveData<List<GitHubRepo>>
        get() = publicReposLiveData

    private var toolbarTitleLiveData = MutableLiveData<Int>()
    val toolbarTitle: LiveData<Int>
        get() = toolbarTitleLiveData

    @StringRes
    var currentUserButtonModeRes: Int? = null

    fun setToolbarTitle(@StringRes title: Int) {
        toolbarTitleLiveData.postValue(title)
    }


    fun getCurrentUser() {

        fragmentScope2.launch {
            val deferredResult = async {
                try {
                    repository.getCurrentUser()
                }catch (t:Throwable){
                    GitHubUser("","","",0,"","","",0,false)}
            }

            val deferredResult2 = async<List<GitHubUser>> {
                try {
                    repository.getCurrentUserFollows()
                }catch (t:Throwable){
                    emptyList<GitHubUser>()
                }
            }

            val user = deferredResult.await()
            val userFollows = deferredResult2.await()


            currentUserLiveData.postValue(user as GitHubUser)
            currentUserFollowsLiveData.postValue(userFollows as List<GitHubUser>)

        }

    }

    fun setCurrentRepo(position: Int) {
        currentRepoLiveData.postValue(publicReposLiveData.value!![position])
    }

    fun getPublicRepos() {
        fragmentScope.launch {
            try {
                val repo = repository.getPublicRepos()
                publicReposLiveData.postValue(repo)
            }catch (t:Throwable){
                publicReposLiveData.postValue(emptyList())
            }
        }
    }

    private fun checkIfRepoStarred(repo: GitHubRepo) {
        viewModelScope.launch {
            try {
                currentRepoLiveData.value!!.starred = repository.checkIfRepoStarred(
                        owner = repo.owner.login,
                        repo = repo.name,
                        onError =  { errorLiveData.postValue(it) })
                currentRepoLiveData.postValue(currentRepoLiveData.value!!)
            }catch (t:Throwable){
                Log.e("Coroutine error", "error description: ", t)
            }
        }
    }

    private fun starRepo(repo: GitHubRepo) {
        viewModelScope.launch {
            try {
                Log.e("AAAAAAAAAa","starRepo in VM work!")
                repository.starRepo(owner = repo.owner.login,
                        repo = repo.name)
                checkIfRepoStarred(repo)
            }catch (t:Throwable){
                Log.e("Coroutine error", "error description: ", t)
            }
        }
    }

    private fun unStarRepo(repo: GitHubRepo) {
        viewModelScope.launch {
            try {
                repository.unStarRepo(owner = repo.owner.login,
                        repo = repo.name)
                checkIfRepoStarred(repo)
            }catch (t:Throwable){
                Log.e("Coroutine error", "error description: ", t)
            }
        }
    }

    fun reverseStar() {
        val currentRepo = currentRepoLiveData.value!!
        when (currentRepo.starred) {
            true -> unStarRepo(currentRepo)
            false -> starRepo(currentRepo)
            else -> starRepo(currentRepo)
        }
    }
}