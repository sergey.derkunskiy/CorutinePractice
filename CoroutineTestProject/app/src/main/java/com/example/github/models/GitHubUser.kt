package com.example.github.models

import com.squareup.moshi.JsonClass
import java.io.Serializable


@JsonClass(generateAdapter = true)
data class GitHubUser(
    val login: String,
    val name: String?,
    val company: String?,
    val id: Int,
    val avatar_url: String,
    val location: String?,
    val email: String?,
    val public_repos: Int?,
    val hireable: Boolean?
): Serializable