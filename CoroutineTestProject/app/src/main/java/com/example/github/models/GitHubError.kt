package com.example.github.models

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class GitHubError(
    val resource: String,
    val code: String,
    val field: String,
    val message: String
)