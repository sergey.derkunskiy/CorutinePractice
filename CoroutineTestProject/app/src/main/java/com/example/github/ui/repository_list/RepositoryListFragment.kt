package com.example.github.ui.repository_list

import android.os.Bundle
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.observe
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.github.R
import com.example.github.RepoRecycleViewAdapter
import com.example.github.models.GithubViewModelMain
import kotlinx.android.synthetic.main.fragment_repository_list.*

class RepositoryListFragment: Fragment(R.layout.fragment_repository_list) {
    private var reposAdapter: RepoRecycleViewAdapter? = null

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        GithubViewModelMain.publicRepos.observe(viewLifecycleOwner) { reposAdapter?.updateGitHubRepo(it)
        }

        GithubViewModelMain.error.observe(viewLifecycleOwner) {
            Toast.makeText(context, it, Toast.LENGTH_LONG).show()
        }

        initList()
        if (savedInstanceState == null) search()
    }

    private fun search() {
        GithubViewModelMain.getPublicRepos()
    }

    private fun initList() {
        reposAdapter = RepoRecycleViewAdapter { position ->
            showRepoDetails(position)
        }
        val divider = DividerItemDecoration(context, DividerItemDecoration.VERTICAL)
        with(reposList) {
            adapter = reposAdapter
            addItemDecoration(divider)
            val linearLayoutManager = LinearLayoutManager(context)
            layoutManager = linearLayoutManager

        }
    }

    private fun showRepoDetails(position: Int) {
        GithubViewModelMain.setCurrentRepo(position)
        val newDialog =
                RepoDetailFragment()
        newDialog.show(childFragmentManager, "detailsRepo_dialog")
    }
}