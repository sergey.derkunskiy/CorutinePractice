package com.example.github.models

import com.squareup.moshi.JsonClass
import java.io.Serializable



@JsonClass(generateAdapter = true)
data class GitHubRepo(
    val id: Int,
    val name: String,
    val full_name: String,
    val description: String?,
    val owner: GitHubUser,
    val html_url: String,
    val ssh_url: String?,
    var starred: Boolean?
): Serializable