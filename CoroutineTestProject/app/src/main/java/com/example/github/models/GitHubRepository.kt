package com.example.github.models

import android.util.Log
import com.example.github.Application
import com.example.github.R
import com.example.github.data.Network
import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Moshi
import kotlinx.coroutines.*
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException

class GitHubRepository {

    private val moshi: Moshi = Moshi.Builder().build()
    private val scope = CoroutineScope(Dispatchers.Default)
    private val adapter: JsonAdapter<GitHubErrorResponse> =
        moshi.adapter(GitHubErrorResponse::class.java).nonNull()


    suspend fun getPublicRepos() : List<GitHubRepo> {
        return withContext(Dispatchers.Default){
            Network.gitHubApi.getReposList()
        }
    }

    suspend fun getCurrentUser() : GitHubUser {
        return Network.gitHubApi.getCurrentUser()
    }

    suspend fun getCurrentUserFollows() : List<GitHubUser> {
        return Network.gitHubApi.getCurrentUserFollows()
    }

    suspend fun checkIfRepoStarred(
            owner: String,
            repo: String,
            onError: (String) -> Unit
    ): Boolean {
        Log.e("AAAAAAAAAa","checkIfRepoStarred work!")
        return suspendCancellableCoroutine<Boolean> { continuation ->
            val call = Network.gitHubApi.checkIfRepoStarred(owner, repo)
            continuation.invokeOnCancellation {
                call.cancel()
            }

            call.enqueue(
                    object : Callback<ResponseBody?> {
                        override fun onFailure(call: Call<ResponseBody?>, t: Throwable) {
                            continuation.resumeWithException(t)
                        }

                        override fun onResponse(
                                call: Call<ResponseBody?>,
                                response: Response<ResponseBody?>
                        ) {
                            return when (response.code()) {
                                204 ->  continuation.resume(true)
                                404 ->  continuation.resume(false)
                                else -> continuation.resumeWithException(error(Application.context!!.getString(R.string.wrong_server_response)))
                            }
                        }
                    }
            )
        }
    }

    suspend fun starRepo(
        owner: String,
        repo: String
    ) {
        Log.e("AAAAAAAAAa","starRepo in repo work!")
        Network.gitHubApi.starRepo(owner, repo)
    }

    suspend fun unStarRepo(
        owner: String,
        repo: String
    ) {
        Network.gitHubApi.unStarRepo(owner, repo)
    }


    private fun parseGitHubErrorResponse(response: String?): String? {
        var errorResponse: GitHubErrorResponse? = null
        try {
            errorResponse = adapter.fromJson(response.orEmpty())
        } catch (e: Exception) {
            Log.d("Moshi parser", e.message.orEmpty())
        }
        var errorMessage: String? = null
        errorResponse?.let { githubResponse ->
            errorMessage = githubResponse.message
            githubResponse.errors?.forEach { githubError ->
                errorMessage += "\n ${githubError.message}"
            }
        }
        return errorMessage
    }

}