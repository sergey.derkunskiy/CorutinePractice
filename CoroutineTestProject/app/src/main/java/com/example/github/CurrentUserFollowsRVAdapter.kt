package com.example.github

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.github.models.GitHubUser
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.follows_item.*
import kotlinx.android.synthetic.main.follows_item.avatar_imageView
import kotlinx.android.synthetic.main.follows_item.id_text
import kotlinx.android.synthetic.main.follows_item.login


class CurrentUserFollowsRVAdapter() : RecyclerView.Adapter<CurrentUserFollowsRVAdapter.ViewHolder>() {

    private val differ = AsyncListDiffer<GitHubUser>(this, FollowsDiffUtilCallback())

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.follows_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = differ.currentList[position]
        holder.bind(item)
    }

    override fun getItemCount(): Int = differ.currentList.size

    inner class ViewHolder(override val containerView: View) : RecyclerView.ViewHolder(containerView), LayoutContainer {

        fun bind(user: GitHubUser) {

            Glide.with(avatar_imageView)
                    .load(user.avatar_url)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .placeholder(R.drawable.ic_avatar)
                    .error(R.drawable.ic_avatar)
                    .into(avatar_imageView)
            login.text = user.login
            id_text.text = user.id.toString()
            name_rep.text =user.public_repos?.toString()
            full_name.text =user.name
            url_text.text =user.company
            city.text =user.location
            description.text =user.email

        }
    }

    fun updateUsersFollows(usersFollows:List<GitHubUser>){
        differ.submitList(usersFollows)
    }

    class FollowsDiffUtilCallback: DiffUtil.ItemCallback<GitHubUser>(){
        override fun areItemsTheSame(oldItem: GitHubUser, newItem: GitHubUser): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: GitHubUser, newItem: GitHubUser): Boolean {
            return newItem == oldItem
        }
    }
}