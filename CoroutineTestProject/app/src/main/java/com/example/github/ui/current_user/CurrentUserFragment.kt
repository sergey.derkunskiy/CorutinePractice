package com.example.github.ui.current_user

import android.os.Bundle
import android.util.Log
import android.widget.AutoCompleteTextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.observe
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.github.CurrentUserFollowsRVAdapter
import com.example.github.R
import com.example.github.models.GithubViewModelMain
import com.example.github.models.GitHubUser
import kotlinx.android.synthetic.main.current_user_fragment.*

class CurrentUserFragment: Fragment(R.layout.current_user_fragment) {
    private lateinit var dropDownList: AutoCompleteTextView
    private var currentUser: GitHubUser? = null
    private var currentUserFollows: List<GitHubUser>? = null
    private var followsAdapter: CurrentUserFollowsRVAdapter? = null

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initList()

        GithubViewModelMain.currentUser.observe(viewLifecycleOwner) { currentUser ->
            setUserToUI(currentUser)
            this.currentUser = currentUser
        }

        GithubViewModelMain.currentUserFollows.observe(viewLifecycleOwner) { currentUserFollows ->
            followsAdapter?.updateUsersFollows(currentUserFollows)
            this.currentUserFollows = currentUserFollows
        }

        GithubViewModelMain.error.observe(viewLifecycleOwner) { errorString ->
            Toast.makeText(context, errorString, Toast.LENGTH_LONG).show()
            currentUser?.let { setUserToUI(it) }
        }

        if (savedInstanceState == null)
            GithubViewModelMain.getCurrentUser()

        avatar_imageView.setOnClickListener {
            Log.e("AAA", currentUserFollows.toString())
            followsAdapter?.updateUsersFollows(currentUserFollows!!)
        }

    }


    private fun setUserToUI(currentUser: GitHubUser) {
        Glide.with(this)
                .load(currentUser.avatar_url)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .placeholder(R.drawable.ic_avatar)
                .error(R.drawable.ic_avatar)
                .into(avatar_imageView)
        login.text = currentUser.login
        id_text.text = currentUser.id.toString()
        name_rep.text =currentUser.public_repos?.toString()
        full_name.text =currentUser.name
        url_text.text =currentUser.company
        city.text =currentUser.location
        description.text =currentUser.email
        when (currentUser.hireable) {
            true -> is_need_work.text = resources.getStringArray(R.array.is_work_array)[0]
            false -> is_need_work.text = resources.getStringArray(R.array.is_work_array)[1]
            null -> is_need_work.text = resources.getStringArray(R.array.is_work_array)[1]
        }

    }

    private fun initList() {
        followsAdapter = CurrentUserFollowsRVAdapter()
        val divider = DividerItemDecoration(context, DividerItemDecoration.VERTICAL)
        with(followsList) {
            adapter = followsAdapter
            addItemDecoration(divider)
            val linearLayoutManager = LinearLayoutManager(context)
            layoutManager = linearLayoutManager

        }
    }


}