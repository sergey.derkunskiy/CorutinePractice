package com.example.github

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.github.models.GitHubRepo
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.repo_item.*

class RepoRecycleViewAdapter(private val onItemClicked: (position: Int) -> Unit) : RecyclerView.Adapter<RepoRecycleViewAdapter.ViewHolder>() {

    private val differ = AsyncListDiffer<GitHubRepo>(this,MovieDiffUtilCallback())

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.repo_item, parent, false)
        return ViewHolder(view,onItemClicked)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = differ.currentList[position]
        holder.bind(item)
    }

    override fun getItemCount(): Int = differ.currentList.size

    inner class ViewHolder(override val containerView: View, onItemClicked: (Int) -> Unit) : RecyclerView.ViewHolder(containerView), LayoutContainer {

        init {
            containerView.setOnClickListener { onItemClicked(adapterPosition) }
        }


        fun bind(repo: GitHubRepo) {
            ownerAvatarIV.isVisible = true
            Glide.with(itemView)
                    .load(repo.owner.avatar_url)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .placeholder(R.drawable.ic_avatar)
                    .error(R.drawable.ic_avatar)
                    .into(ownerAvatarIV)

            repoNameTV.text = repo.name
            repoDescTV.text = repo.description
            repoIdTV.text = repo.id.toString()

        }
    }

    fun updateGitHubRepo(gitHubRepos:List<GitHubRepo>){
        differ.submitList(gitHubRepos)
    }

    class MovieDiffUtilCallback: DiffUtil.ItemCallback<GitHubRepo>(){
        override fun areItemsTheSame(oldItem: GitHubRepo, newItem: GitHubRepo): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: GitHubRepo, newItem: GitHubRepo): Boolean {
            return newItem == oldItem
        }
    }
}