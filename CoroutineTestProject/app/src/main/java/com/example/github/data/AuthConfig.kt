package com.example.github.data

import net.openid.appauth.ResponseTypeValues

object AuthConfig {

    const val AUTH_URI = "https://github.com/login/oauth/authorize"
    const val TOKEN_URI = "https://github.com/login/oauth/access_token"
    const val RESPONSE_TYPE = ResponseTypeValues.CODE
    const val SCOPE = "user,repo"

    const val CLIENT_ID = "77854a65ee0bdb5e5789"
    const val CLIENT_SECRET = "1e44bcb7cce5510241533e6cc7caa8d75caf011a"
    const val CALLBACK_URL = "skillbox://skillbox.ru/callback"
}